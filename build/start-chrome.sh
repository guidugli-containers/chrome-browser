#!/bin/bash

mkdir -p /home/$USER_NAME /.cache
chown $USER_NAME:$USER_NAME /home/$USER_NAME /.cache
chmod 750 /home/$USER_NAME /.cache
chown $USER_NAME:$USER_NAME $XDG_RUNTIME_DIR
chmod 750 $XDG_RUNTIME_DIR

for i in `ls -a $HOME`; do chown $USER_NAME:$USER_NAME $HOME/$i; done

cd /home/$USER_NAME

exec su $USER_NAME -c "HOME=/home/$USER_NAME /opt/google/chrome/chrome $ARGS"
